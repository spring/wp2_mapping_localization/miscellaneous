#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/QuaternionStamped.h>
#include<std_msgs/String.h>
#include<math.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include <vector>
#include <string>


// enum color { RED, GREEN, BLUE } c = RED, *cp = &c;
std::vector<std::string> links {"front_fisheye_camera_optical_frame", "head_front_camera_optical_frame", 
"rear_fisheye_camera_optical_frame", "torso_back_camera_fisheye1_optical_frame", "torso_back_camera_fisheye1_optical_frame", 
"torso_front_camera_color_optical_frame", "torso_front_camera_infra1_optical_frame", "torso_front_camera_infra2_optical_frame"};

geometry_msgs::PoseWithCovarianceStamped robot_pose;

void robot_pose_callback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
    robot_pose = *msg;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "camera_poses");
    ros::NodeHandle node;
    ros::Subscriber robot_pose_sub = node.subscribe<geometry_msgs::PoseWithCovarianceStamped> ("/robot_pose", 100, robot_pose_callback);
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);

    // ros::Rate rate(10.0);
    while(node.ok())
    {
        for(size_t i=0;i<links.size();i++)
        {   
            geometry_msgs::PoseWithCovarianceStamped transformStamped;
            try
            {
                transformStamped = tfBuffer.transform(robot_pose, links.at(i));               
            }
            catch (tf2::TransformException &ex) 
            {
                ROS_WARN("%s",ex.what());
                ros::Duration(1.0).sleep();
                continue;
            }
        //  std::cout<<transformStamped.pose.pose.position.x<<std::endl;   
        }
        ros::spinOnce();
    }
}