#!/usr/bin/env python

import rospy
import numpy as np
from pathlib import Path
import cv2
import sys
import roslib
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

count=1
def compressed_callback(msg):
    global count, last
    time = msg.header.stamp.secs
    try:
        np_arr = np.frombuffer(msg.data, np.uint8)
        image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        cv2.imshow("fisheye image", cv2.resize(image, (1000, 1000)) )
        cv2.waitKey(1)
        if count==1 or (time - last) > 3:
            cv2.imwrite(str(count)+'.jpg', image)
            rospy.loginfo("Saved image")
            count+=1
            last = time
    except Exception as E:
        rospy.logwarn(E)
        pass



rospy.init_node('image_viewer')
br = CvBridge()
# topic = "/front_fisheye_camera/image_raw/compressed"
topic = "/rear_fisheye_camera/image_raw/compressed"

last = 0#rospy.Time.now()
sub = rospy.Subscriber(topic, CompressedImage, compressed_callback, queue_size=10)

rospy.spin()
