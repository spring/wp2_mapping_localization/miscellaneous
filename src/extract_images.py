#!/usr/bin/env python

from logging import exception
import rospy
import numpy as np
from pathlib import Path
import cv2
import sys
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from geometry_msgs.msg import PoseWithCovarianceStamped, Point, Quaternion 
from cv_bridge import CvBridge
import subprocess as sp 
import json
import tf 
import argparse

position_diff_thresh = 0.1 # m 
orientation_diff_thresh = 10 # degree

images_folder = False

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--bag_file', type=str, required=False, default='', help="Path for the bag file. Leave empty if extracting from live topics")
    home_folder = Path.home()
    save_folder_default = home_folder.joinpath("images")
    parser.add_argument('--save_folder', type=str, required=False, default=save_folder_default, help="Path for the outer directory where images are saved. Default /home/USER/images/")
    parser.add_argument('--pose_topic', type=str, required=False, default="/robot_pose", help="Pose topic.")
    args = parser.parse_args()
    return args

args = parse_args()

bag_file = args.bag_file
save_folder = args.save_folder

pose_file = str(save_folder)+'/robot_pose.json'
pose_list = []
try:
    player_proc = sp.Popen(['rosbag', 'play', bag_file])
except:
    rospy.logwarn("No bag file provided or cannot play bag. Either play the bag file with `rosbag play` in a separate terminal, or make sure you can access live camera topis")

robot_position = None
robot_orientation = None

def on_shut():
    rospy.loginfo("SHUTTING DOWN")
    with open(pose_file, 'w') as fout:
        json.dump(pose_list, fout)

def pose_callback(msg):
    global robot_position, robot_orientation, pose_list
    pose_dict = {}
    time = msg.header.stamp
    robot_position = [msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z]
    robot_orientation = [msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w]
    pose_dict['position'] = [msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z]
    pose_dict['orientation'] = [msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w]
    pose_dict['time'] = str(time)
    pose_list.append(pose_dict)

class Camera():
    def __init__(self, camera):
        self.last_position = [0,0,0]
        self.last_orientation = [0,0,0,1]

        self.br = CvBridge()
        self.camera_name = camera[0]
        self.inner_folder =  str(save_folder)+'/'+self.camera_name
        self.inner_folder_path = Path(self.inner_folder)
        try:
            self.inner_folder_path.mkdir(parents=True,exist_ok=True)
        except:
            self.inner_folder_path.mkdir(parents=True)

        self.topic = camera[1][0]
        self.compressed = camera[1][1]
        if self.compressed==1:
            self.topic = self.topic+'/compressed'
            self.sub = rospy.Subscriber(self.topic, CompressedImage, self.compressed_callback, queue_size=10)
        else:
            self.sub = rospy.Subscriber(self.topic, Image, self.image_callback, queue_size=10)                
        
    def compressed_callback(self,msg):
        global robot_position, robot_orientation
        time = msg.header.stamp
        if robot_position is None or robot_orientation is None:
            try:
                np_arr = np.frombuffer(msg.data, np.uint8)
                image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
                cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
            except:
                pass
        else:

            position_diff = np.linalg.norm(np.asarray(self.last_position) - np.asarray(robot_position))
            q1_inv = [0]*4
            q2 = [0]*4 
            q1_inv[0] = self.last_orientation[0]
            q1_inv[1] = self.last_orientation[1]
            q1_inv[2] = self.last_orientation[2]
            q1_inv[3] = -self.last_orientation[3] # Negate for inverse
        
            q2[0] = robot_orientation[0]
            q2[1] = robot_orientation[1]
            q2[2] = robot_orientation[2]
            q2[3] = robot_orientation[3]
            
            relative_orientation = tf.transformations.quaternion_multiply(q2, q1_inv)
            relative_orientation_euler_deg = np.rad2deg(tf.transformations.euler_from_quaternion(relative_orientation))
            if position_diff > position_diff_thresh or np.linalg.norm(relative_orientation_euler_deg) > orientation_diff_thresh:
                try:
                    np_arr = np.frombuffer(msg.data, np.uint8)
                    image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
                    cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
                except:
                    pass

                self.last_position = robot_position
                self.last_orientation = robot_orientation                

    def image_callback(self,msg):
        global robot_position, robot_orientation
        time = msg.header.stamp
        if robot_position is None or robot_orientation is None:
            try:
                image = self.br.imgmsg_to_cv2(msg)
                cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
            except:
                pass
        else: 
            position_diff = np.linalg.norm(self.last_position - robot_position)
            q1_inv = []
            q2 = [] 
            q1_inv[0] = self.last_orientation.x
            q1_inv[1] = self.last_orientation.y
            q1_inv[2] = self.last_orientation.z
            q1_inv[3] = -self.last_orientation.w # Negate for inverse
        
            q2[0] = robot_orientation.x
            q2[1] = robot_orientation.y
            q2[2] = robot_orientation.z
            q2[3] = robot_orientation.w
            
            relative_orientation = tf.transformations.quaternion_multiply(q2, q1_inv)
            relative_orientation_euler_deg = np.rad2deg(tf.transformations.euler_from_quaternion(relative_orientation))
            
            if position_diff > position_diff_thresh or relative_orientation_euler_deg > orientation_diff_thresh:
                try:
                    image = self.br.imgmsg_to_cv2(msg)
                    cv2.imwrite(self.inner_folder+'/'+str(time)+'.jpg', image)
                except:
                    pass



cameras = {
 'head_front':('/head_front_camera/image_raw',1),
 'front_fisheye':('/front_fisheye_camera/image_raw',1),
 'torso_front_color':('/torso_front_camera/color/image_raw',1),
 'torso_frontIR1':('/torso_front_camera/infra1/image_rect_raw',1),  
 'torso_frontIR2':('/torso_front_camera/infra2/image_rect_raw',1),
 'rear_fisheye':('/rear_camera/fisheye/image_raw',1), 
 'torso_back1':('/torso_back_camera/fisheye1/image_raw',1), 
 'torso_back2':('/torso_back_camera/fisheye2/image_raw',1) 
 }
cameras_list = list(cameras.keys())
pose_topic = args.pose_topic
pose_sub = rospy.Subscriber(pose_topic, PoseWithCovarianceStamped, pose_callback, queue_size=10)


if __name__=='__main__':
    rospy.init_node('image_extractor')
    Cams = []
    for i in range(len(cameras_list)):
        camera = cameras_list[i]
        topic, compressed =  cameras[camera]    
        cam = Camera((camera, (topic,compressed)))
        Cams.append(cam)
        
    while(player_proc.poll() is None):
        pass
    rospy.loginfo("Bag file ended. Shut down now")        

    rospy.signal_shutdown("Bag file ended")

    rospy.on_shutdown(on_shut)
    # rospy.spin()