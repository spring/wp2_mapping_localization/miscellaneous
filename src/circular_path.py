#!/usr/bin/env python
import rospy
import numpy as np
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped 
from nav_msgs.msg import Path 
import tf 
import sys
import argparse

robot_pose = PoseStamped()

def robot_pose_subscriber(msg):
    global robot_pose
    robot_pose.pose.position = msg.pose.pose.position
    robot_pose.pose.orientation = msg.pose.pose.orientation

def get_relative_motion(robot_pose, goal_pose):
    robot_position = np.asarray([robot_pose.pose.position.x, robot_pose.pose.position.y, robot_pose.pose.position.z]) 
    goal_position = np.asarray([goal_pose.pose.position.x, goal_pose.pose.position.y, goal_pose.pose.position.z]) 

    position_diff = np.linalg.norm(robot_position - goal_position)

    q1_inv = [0]*4
    q2 = [0]*4 
    q1_inv[0] = robot_pose.pose.orientation.x
    q1_inv[1] = robot_pose.pose.orientation.y
    q1_inv[2] = robot_pose.pose.orientation.z
    q1_inv[3] = -robot_pose.pose.orientation.w # Negate for inverse

    q2[0] = goal_pose.pose.orientation.x
    q2[1] = goal_pose.pose.orientation.y
    q2[2] = goal_pose.pose.orientation.z
    q2[3] = goal_pose.pose.orientation.w

    relative_orientation = tf.transformations.quaternion_multiply(q2, q1_inv)
    relative_orientation_euler_deg = np.linalg.norm(np.rad2deg(tf.transformations.euler_from_quaternion(relative_orientation)))
    return position_diff, relative_orientation_euler_deg    

def get_circular_path(center_x, center_y, radii=[0,0.5,1], theta_increment=np.pi/6, dist_threshold=0.15, angle_threshold=5, sleep_duration=1, timeout=10):
    '''
    Get poses along concentric circles based on r*unit circle paramaterization. 
    '''
    global robot_pose
    timeout_duration = rospy.Duration(timeout)

    thetas = np.arange(0, 2*np.pi, theta_increment)
    circular_path = Path()
    poses = []
    ct=0

    for r in radii:
        for theta in thetas:
            pose = PoseStamped()        
            # x,y if center was at (0,0)
            x_circle = r*np.cos(theta)
            y_circle = r*np.sin(theta)

            # x,y for given center in map.  
            x_map = center_x + x_circle
            y_map = center_y + y_circle
            z_map = 0 
            
            # angle along Z-axis
            theta_map = np.pi + theta # to look at the center of the circle
            axis = np.asarray([0,0,1]).reshape((3,1))
            # Axis-angle to quaternion
            quat = np.vstack( (axis*np.sin(theta_map/2),np.cos(theta_map/2))  ).reshape((4,))
    
            pose.header.seq = ct
            pose.header.stamp = rospy.Time.now()
            pose.header.frame_id = "map"
            pose.pose.orientation.x = quat[0]
            pose.pose.orientation.y = quat[1]
            pose.pose.orientation.z = quat[2]
            pose.pose.orientation.w = quat[3]
    
            pose.pose.position.x = x_map
            pose.pose.position.y = y_map
            pose.pose.position.z = z_map

            poses.append(pose)
            ct += 1

    # For visualization of the path 
    circular_path.header.stamp = rospy.Time.now()
    circular_path.header.frame_id = "map"
    circular_path.poses = poses
    path_pub.publish(circular_path)
    
    for pose in poses:
        # Send pose to /move_base_simple/goal
        goal_pub.publish(pose)
        
        # Needed to exit process on ctrl+c. Otherwise, waits for the for loop to end before shutting down.
        if rospy.is_shutdown():
            continue

        # Get, and compare robot current pose to goal pose until within thresholds. 
        rel_dist, rel_angle = get_relative_motion(robot_pose, pose)
        time_st = rospy.Time.now()
        time_now = rospy.Time.now()

        while( not rospy.is_shutdown() and (rel_dist > dist_threshold or rel_angle > angle_threshold) and ((time_now - time_st) <= timeout_duration ) ):
            rel_dist, rel_angle = get_relative_motion(robot_pose, pose)
            time_now = rospy.Time.now()

        if rel_angle > angle_threshold or rel_dist > dist_threshold:
            rospy.logwarn(f"Timed out. Did not or could not reach required position exactly (within threshold) in {timeout} seconds")
        else:
            rospy.loginfo(f"Reached pose. Sleep for {sleep_duration} s")
        # Sleep when robot is at required pose along a circle. 
        rospy.sleep(sleep_duration)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-x', '--x_map',            type=str, required=False, default='0', help="x position of center of circles. Default 0")
    parser.add_argument('-y', '--y_map',            type=str, required=False, default='0', help="y position of center of circles. Default 0")
    parser.add_argument('--radii',                  type=str, required=False, default='0.1 0.25 0.4', help="radii for concentric circles (in m). Enter radii in format: 'r1 r2 r3' (WITH quotes) ")
    parser.add_argument('-theta', '--theta_inc',    type=str, required=False, default=str(np.pi/6), help="Theta increment in the unit circle.Default: pi/6")
    parser.add_argument('-d', '--dist_thresh',      type=str, required=False, default='0.15', help="Distance threshold for path follower. Default: 0.05")
    parser.add_argument('-a', '--angle_thresh',     type=str, required=False, default='5', help="Angle threshold (in degrees) for path follower. Default: 5")
    parser.add_argument('-s', '--sleep_duration',   type=str, required=False, default='1', help="Sleep duration at pose. Default: 1s")
    parser.add_argument('-rp', '--robot_pose',      type=str, required=False, default='/robot_pose', help="Robot pose topic to subscribe to")
    parser.add_argument('-p', '--path_topic',       type=str, required=False, default='/circle_generator/plan', help="Topic where the path is sent for visualization")
    parser.add_argument('-g', '--goal_topic',       type=str, required=False, default='/move_base_simple/goal', help="Topic where each goal is sent")
    parser.add_argument('-t', '--time_out',         type=str, required=False, default='10', help="Timeout duration in seconds")
    
    args = parser.parse_args()
    return args

def shut():
    rospy.loginfo("shutting down node")
    exit()
    
if __name__ == '__main__':
    # Parse arguments
    args = parse_args()
    x = float(args.x_map)
    y = float(args.y_map)
    radii = args.radii.split(' ')
    radii = [float(r) for r in radii]
    theta_inc = float(args.theta_inc)
    dist_thresh = float(args.dist_thresh)
    angle_thresh = float(args.angle_thresh)
    sleep_duration = float(args.sleep_duration)
    timeOut = float(args.time_out)
    
    # Initialize ROS stuff
    rospy.init_node("circle_generator")
    pose_sub = rospy.Subscriber(args.robot_pose, PoseWithCovarianceStamped, robot_pose_subscriber, queue_size=10)
    path_pub = rospy.Publisher(args.path_topic, Path, queue_size=10, latch=True)
    goal_pub = rospy.Publisher(args.goal_topic, PoseStamped, queue_size=10, latch=True)
    rospy.loginfo("Starting concentric circles planner, and follower.")
    get_circular_path(x, y, radii, theta_inc, dist_threshold=dist_thresh, angle_threshold=angle_thresh, sleep_duration=sleep_duration, timeout=timeOut)
    rospy.spin()
    rospy.on_shutdown(shut)

