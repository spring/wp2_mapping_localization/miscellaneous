## Description
Miscellaneous scripts for ARI running either on ARI, or on data from ARI. 
Current version includes fisheye calibration and covenience scripts for visualizing and capturing fisheye images, and a script to extract timestamped images from all the cameras from either a rosbag or live topics  
## Installation
1. Run `catkin build` for the ros scripts.  
2. To use the fisheye camera calibration module: 
* `cd src/fisheye_calibration`
* Compile with g++ including the opencv library flags.  

## Usage
1. The extract_images script extracts images (raw or compressed) from either a provided rosbag or from live topics.  
* Run with `rosrun ari_misc extract_images.py <save_folder>` to save images from live topics, where the `save_folder` is the directory where you want your images to be saved. If no save_folder is given, it takes the home directory as the default. Images are saved in the structure `save_folder/<camera_name>/<timestamp>.jpg`
* Run with `rosrun ari_misc extract_images.py <save_folder> <bag_file>` to save images from a recorded bag file. In this case, both arguments are needed. 

If the robot pose data is available, not every image is extracted, but it is done so when there is a threshold (rotation and orientation) difference in poses. This is required for some applications, but if not necessarry, change `position_diff_thresh = 0 and orientation_diff_thresh = 0`.  
 
By default, it uses the compressed image topics, but for the raw images, in the `cameras` dictionary, change the 2nd value in the tuple to 0 for the respective topic. 

2. For the fisheye calibration:
* Capture images using the `fisheye_view.py` script with your pattern. 
* Include the image paths in `src/fisheye_calibration/configFiles_calib/image_files.xml`
* Edit the `src/fisheye_calibration/configFiles_calib/params.xml` for your purposes.
* Run the compiled program from `calibrate_fisheye.cpp` with the `params.xml` as an argument.

## Project status
Ongoing
